;; Ez egy emacs init.el fáj, aminek legfrissebb verziója innen letölthető:
;; https://gitlab.com/atmamta2/emacs_init.el git repoval,

;; ______________emacs telepítése
;; 0. updateld és upgradeld az OS-edet a legfrissebb stabil verzióra (ha linux vagy MAC OS...)
;; 1. a legtöbb linux OS-ben ezt az
;; egy parancssort kell kiadni terminálból: 
;; >>> sudo apt-get install emacs
;; (MINDEN ITT SZEREPLŐ TERMINÁL PARANCS benne van az install_scriptben, ami
;; miután végrhajthatóvá teszünk a chmod paranccsal, így futtatható:
;; >>> ./install_script)
;; Sometimes the OS version is emacs is way behind the latest emacs, which may
;; cause problems. Here is how to get emacs25 on linux (at the time of writing this
;; the linux mint official stable emacs version is 24.x):
;; >>> sudo apt-get install emacs25
;; and then copy the /usr/bin/emacs25 launcher script to /usr/bin/emacs
;; ...and here is how to get emacs snapshot (i.e. the latest version) on linux
;; (which is probably not necessary):
;; >>> sudo add-apt-repository -y ppa:ubuntu-elisp
;; >>> sudo apt-get update
;; >>> sudo apt-get install emacs-snapshot
;; After doing this you would need to launch emacs with the
;; >>> emacs-snapshot
;; command (instead of >>> emacs).

;; 
;; 2. Ez után ezt az emacs init.el fájl, amit jelenleg olvasol
;; (linux OS-ben) ide el kéne menteni: ~/.emacs.d/init.el 
;; mac-en lehet, hogy máshova, a következő linken tuti le van írva..  Mac
;; leírás: www.emacswiki.org/emacs/EmacsForMacOS windowson is hasonló
;; lépések szükségesek, de ott még egy home dir változót is létre kell
;; hozni emlékeim szering, de ott neked kell utánanézned (egy google
;; keresés... :-)  + lásd windows_init.el fájl ugyanobban a mappában, ahol
;; ez a fájl is található.

;; 3. Keress egy szimpatikus emacs cheat sheetet, nyomtasd ki, és ragaszd
;; ki a falra magad mellé, hogy mindig ott legyen. Pl itt egy:
;; www.gnu.org/software/emacs/refcards/pdf/refcard.pdf

;; elpy a python IDE package (az egyik.. sztem a legjobb jelenleg),
;; amit installálnod kéne az emacs-on belül, az ein meg a notebook,
;; ami állítólag működik a jupyterrel (de ez utóbbit én sose
;; próbáltam)

;; A továbbiák feltételezik az anaconda python csomag meglétét.
;; 4. 
;; >>> conda update conda
;; >>> conda install pip; conda update pip; pip install pip --upgrade
;; >>> conda update numpy

;;___________init.el user-specific customization:
;; search for for "AAA" in this init file, and edit the 
;; variables preceeding it to suit your environment!



;;____________ elpy setup:
;; 1. run this commands from a terminal:
;; >>> pip install elpy jedi rope importmagic autopep8 yapf flake8 cl epc
;; >>> sudo apt-get install python-autopep8
;; 2. in a freshly started emacs, run this command:
;; M-x elpy-config
;; and check if everything needed works. 
;; If you dont like the flymake (based on flake8) syntax checker highlighter
;; here is how to disable it:
;; 3. C-h v  (this command lets you inspect and change any emacs variable), then
;; enter the 'elpy-modules' variable name and hit enter, and uncheck flymake
;; in the resulting window. 
;; 4. If you get mkl or other errors while using elpy in emacs, thse steps will
;; likely fix it:
;; conda update conda
;; conda update pip
;; conda update numpy

;;___________ein (emacs-ipython-notebook) dokumentáció:
;; tkf.github.io/emacs-ipython-notebook/
;; emacs, ipython + jupiter setup explained:
;; realpython.com/blog/python/emacs-the-best-python-editor/


;; __________általános hibakezelés: 
;; 1. szinkronizálj a  https://gitlab.com/atmamta2/emacs_init.el git repoval,
;; hátha nekem is voltak ugyanezzel gondjaim, s már ki is javítottam őket..
;; 2. nézd meg mit mondanak a
;; *Messages*, meg a *Warnings* emacs bufferek (az utóbbi nem mindig
;; létezik..). Ha hiányzik vagy problémás egy csomag, próbáld meg
;; manuálisan installálni az M-x install-package paranccsal, ha emacs
;; csomagról van szó, ha python csomag, akkor pip-pel vagy conda-val,
;; ha ez sem segít, akkor updateld a conda-t, pip-et, és végső soron
;; az OS-t és a panaszos csomagot. Az is lehet, hogy nem az emacs
;; csomag hiányol pl egy python csomagot, hanem maga az emacs. Ha ez a
;; helyzet, és egy külön (pl anacondás) pythonból futtatod az elpy-t,
;; meg a python interpretert, akkor azt a OS /usr/bin/emacs-ből futó
;; emacs nem látja, mert ő a /usr/bin/python-t használja. Ezért az
;; lehet a végső megoldás, hogy az OS normál csomagkezelőjével (is)
;; installálod vagy updateled azt a csomagot, amiről panaszkodik az
;; emacs.

;; Recompiling, i.e. getting the already installed packages to work
;; after upgrading to another major emacs version
;; (i.e. say from 24.1 to 25.3):
;; 1. upgrade conda, and all relevant installed pip python packages, then
;; do either of these (I recommend 2.b):
;; 2.a. The easiest approach could be to move your whole ~/.emacs.d/elpa
;; directory to a backup location, and reinstall elpy and your other
;; packages from scratch *after* you have upgraded your emacs and related
;; python packages.
;; 2.b. Alternative: remove all compiled elisp files (which forces recompilation
;; later):
;; cd ~/.emacs.d/
;; find . -name '*.elc' | xargs rm
;; 2.c. upgrade your existing emacs lisp packages in place:
;; Put the following emacs lisp code
;; into a scrap buffer:
;; (defun package-reinstall-activated ()
;;   "Reinstall all activated packages."
;;   (interactive)
;;   (dolist (package-name package-activated-list)
;;     (when (package-installed-p package-name)
;;       (unless (ignore-errors                   ;some packages may fail to install
;;                 (package-reinstall package-name)
;;                 (warn "Package %s failed to reinstall" package-name))))))
;; then run M-x eval-buffer, then run M-x package-reinstall-activated multiple
;; times, until there are no Warning messages about "Package xyz failed to reinstall".
;; 2.c. Alternatively (if the latter would take too long..),
;; just run the above command once, and then run
;; M-x package-reinstall RET package-name
;; manually on the packages that are listed as "failed to reinstall"

;; Settin python project root:  two steps:
;; 1. run M-x elpy-project-root and set the elpy-project-root variable to the desired value
;; 2. customize ppproject-root variable below (which will in turn add its value to
;;                                             variable python-shell-extra-pythonpaths). OR:
;; 3. if your project root is the same as your git root, customize elpy-project-root-finder-functions
;; variable -- choose the one that uses git.
;; OR: All of the above options for setting project root directory make sense only in case you are
;; working on just one python project for a long time... If this is not the case, you can set
;; directory-(or file-)specific variables, see here:
;; www.gnu.org/software/emacs/manual/html_node/emacs/Directory-Variables.html

(require 'package)
(add-to-list 'package-archives
             '("melpa-stable" . "http://melpa-stable.milkbox.net/packages/"))
(add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("marmalade" . "https://marmalade-repo.org/packages/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

(package-initialize)

(defvar my-packages '(
                      ;;el-get   ;; emacs elisp external package manager
                      better-defaults
                      ;;projectile  ;; project management
                      ;; clojure-mode 
                      ;; cider    ;; clojure development environment 
                      ;;ac-cider ;; clojue autocompletion
                      paredit  ;; automatic paranthesis completion
                      python   ;; python mode
                      ;; ein      ;; emacs ipython notebook
                      elpy     ;; advanced python mode
                      flycheck ;; syntax check on the fly
                      ;; pyflakes
                      ;;py-autopep8 ;; python buffer beautifier using autopep
                      ;; egg      ;; emacs got git - git emacs integration
                      anaconda-mode ;; show doc, def, etc at point
                      rainbow-delimiters  ;; highlights the other side of brackets
                      company
                      pyvenv 
                      find-file-in-project
                      s
                      importmagic
                      ))

(dolist (p my-packages)
  (unless (package-installed-p p)
    (package-install p)))

;;_________________clojure_____________________
;; (add-hook 'clojure-mode-hook
;;           '(lambda ()
;;              (paredit-mode 1)
;;              ;; Actual keyboard bindings follow:
;;              ;;Start Cider REPL with control-o-j:
;;              (define-key clojure-mode-map (kbd "C-o j") 'cider-jack-in)
;;              ;;Restart Cider REPL with control-o-J (in case of dirty JVM, etc.):
;;              (define-key clojure-mode-map (kbd "C-o J") 'cider-restart)
;;              (define-key clojure-mode-map (kbd "s-i") 'cider-eval-last-sexp)
;;              (define-key clojure-mode-map (kbd "C-o y") 'cider-eval-last-sexp-and-append)))

;; ;; Append result of evaluating previous expression (Clojure):
;; (defun cider-eval-last-sexp-and-append ()
;;   "Evaluate the expression preceding point and append result."
;;   (interactive)
;;   (with-current-buffer (current-buffer)
;;       (insert ";;=>")
;;       (eval-print-last-sexp)
;;       )
;;     )



;;_________________python elpy_____________________
; note: documentation of elpy mode interactive key bindings are here:
; http://elpy.readthedocs.io/en/latest/ide.html
;; (setq path-to-python-executable "/usr/bin/python")

;; !!!Edit the computer names and the paths to the python executable on these systems in the following!!!:

;; python 3 env:
(setq anaconda-root-dir (expand-file-name "anaconda3" "~")) ;;AAA
;; python 2 env:
(setq anaconda-bin-dir (expand-file-name "bin" anaconda-root-dir)) ;; Linux
(setq python-executable-str1 (expand-file-name "python" anaconda-bin-dir))
(setq python-executable-str2  (expand-file-name "ipython" anaconda-bin-dir)) 
(setq local-bin-dir (expand-file-name "usr/bin/" "~"))  ;;AAA
(setq pydpl-project-root (expand-file-name "work/ml/neural_network_dir/pydpl" "~")) ;;AAA
(setq audio-project-root (expand-file-name "work/ml/neural_network_dir/pydpl/audio" "~")) ;;AAA
(setq vision-project-root (expand-file-name "work/ml/neural_network_dir/pydpl/vision" "~")) ;;AAA

(add-to-list 'exec-path anaconda-bin-dir)  ;;AAA
;(add-to-list 'exec-path vision-project-root) ;;AAA
;(add-to-list 'exec-path audio-project-root) ;;AAA
;(add-to-list 'exec-path local-bin-dir)  ;;AAA


(setq ansi-color-for-comint-mode t)

(setq path-to-python-executable anaconda-bin-dir
      python-shell-interpreter  python-executable-str1 
      ;;python-shell-interpreter-args "-i"
      )

;; (cond ((string= system-name "alfa")
;;        (setq path-to-python-executable python-executable-str2)
;;        ;(pyvenv-activate "/home/attila/usr/local/lib/anaconda3/bin/")
;;        )
;;       ((string= system-name "hulk4")
;;        (setq path-to-python-executable python-executable-str2)
;;        ;(pyvenv-activate "/home/attila/usr/local/lib/anaconda3/bin/")
;;        ))

(add-hook 'python-mode-hook
          '(lambda ()
	     (eldoc-mode 1)
             (elpy-mode 1)))



(add-hook 'elpy-mode-hook
          '(lambda ()
             (paredit-mode 1)
             (anaconda-mode 1)
             (paredit-mode 1)
             (flycheck-mode 1)
             ;(flymake-mode 1)
             ;;(elpy-use-ipython python-executable-str2)
             ;;(elpy-use-cpython python-executable-str1)
	     ))



(elpy-enable)

;;  ;; use flycheck not flymake with elpy
;; (when (require 'flycheck nil t)
;;   (setq elpy-modules (delq 'elpy-module-flymake elpy-modules)))

;; ;; enable autopep8 formatting on save
;; (require 'py-autopep8)
;; (add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)

(defun company-yasnippet-or-completion ()
  "Solve company yasnippet conflicts."
  (interactive)
  (let ((yas-fallback-behavior
         (apply 'company-complete-common nil)))
    (yas-expand)))

(add-hook 'company-mode-hook
          (lambda ()
            (substitute-key-definition
             'company-complete-common
             'company-yasnippet-or-completion
             company-active-map)))




(setq tramp-default-method "ssh")

(defun toggle-fullscreen ()
  (interactive)
  (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
                 '(2 "_NET_WM_STATE_MAXIMIZED_VERT" 0))
  (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
                 '(2 "_NET_WM_STATE_MAXIMIZED_HORZ" 0))
  )
(toggle-fullscreen)


(defun eval-after-restart-python-console ()
  "Restart python console before evaluate buffer or region to avoid various uncanny conflicts, like not reloding modules even when they are changed"
  (interactive)
  (if (get-buffer "*Python*")
      (let ((kill-buffer-query-functions nil)) (kill-buffer "*Python*"))
    (split-window-horizontally (floor (* 0.75 (window-width)))))
  (elpy-shell-send-region-or-buffer)
  (enlarge-window-horizontally 20)
  )


(global-set-key (kbd "C-c C-x C-c") 'eval-after-restart-python-console)


(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:stipple nil :background "black" :foreground "white" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 136 :foundry "PfEd" :family "DejaVu Sans Mono" :width normal))))
 '(font-lock-builtin-face ((((class color) (background dark)) (:foreground "Turquoise"))))
 '(font-lock-comment-face ((t (:foreground "purple"))))
 '(font-lock-constant-face ((((class color) (background dark)) (:bold t :foreground "DarkOrchid"))))
 '(font-lock-doc-string-face ((t (:foreground "green2"))))
 '(font-lock-function-name-face ((t (:foreground "yellow"))))
 '(font-lock-keyword-face ((t (:bold t :foreground "orange"))))
 '(font-lock-preprocessor-face ((t (:italic nil :foreground "CornFlowerBlue"))))
 '(font-lock-reference-face ((t (:foreground "DodgerBlue"))))
 '(font-lock-string-face ((t (:foreground "orange"))))
 '(font-lock-type-face ((t (:foreground "#9290ff"))))
 '(font-lock-variable-name-face ((t (:foreground "PaleGreen"))))
 '(font-lock-warning-face ((((class color) (background dark)) (:foreground "yellow" :background "red"))))
 '(highlight ((t (:background "gray5"))))
 '(list-mode-item-selected ((t (:background "gold"))))
 '(makefile-space-face ((t (:background "wheat"))) t)
 '(mode-line ((t (:background "Navy"))))
 '(paren-match ((t (:background "darkseagreen4"))))
 '(region ((t (:background "DarkSlateBlue"))))
 '(show-paren-match ((t (:foreground "red" :background "black"))))
 '(show-paren-mismatch ((((class color)) (:foreground "white" :background "red"))))
 '(speedbar-button-face ((((class color) (background dark)) (:foreground "green4"))))
 '(speedbar-directory-face ((((class color) (background dark)) (:foreground "khaki"))))
 '(speedbar-file-face ((((class color) (background dark)) (:foreground "cyan"))))
 '(speedbar-tag-face ((((class color) (background dark)) (:foreground "Springgreen"))))
 '(vhdl-speedbar-architecture-selected-face ((((class color) (background dark)) (:underline t :foreground "Blue"))))
 '(vhdl-speedbar-entity-face ((((class color) (background dark)) (:foreground "darkGreen"))))
 '(vhdl-speedbar-entity-selected-face ((((class color) (background dark)) (:underline t :foreground "darkGreen"))))
 '(vhdl-speedbar-package-face ((((class color) (background dark)) (:foreground "black"))))
 '(vhdl-speedbar-package-selected-face ((((class color) (background dark)) (:underline t :foreground "black")))))

(setq my-unignored-buffers '("*scratch*" "*Python*" "*Messages*"))

(defun my-ido-ignore-func (name)
  "Ignore all non-user (a.k.a. *starred*) buffers except those listed in `my-unignored-buffers'."
  (and (string-match "^\*" name)
       (not (member name my-unignored-buffers))))

;(setq ido-ignore-buffers '("\\` " my-ido-ignore-func))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(elpy-modules
   '(elpy-module-company elpy-module-eldoc elpy-module-pyvenv elpy-module-highlight-indentation elpy-module-yasnippet elpy-module-django elpy-module-sane-defaults))
 '(elpy-project-root pydpl-project-root)
 '(elpy-rpc-python-command python-executable-str1)
 '(flymake-gui-warnings-enabled nil)
 '(ido-ignore-buffers '("\\` " my-ido-ignore-func))
 '(menu-bar-mode nil)
 '(package-selected-packages
   '(importmagic elpy websocket tramp-theme spinner rainbow-delimiters queue python py-autopep8 projectile paredit exec-path-from-shell cl-generic better-defaults auto-complete anaconda-mode))
 '(python-indent-offset 4)
 '(show-paren-mode t)
 '(tab-stop-list
   '(4 8 12 16 20 24 28 32 36 40 44 48 52 56 64 72 80 88 96 104 112 120))
 '(tool-bar-mode nil))

(load-file (expand-file-name ".emacs.d/elpa/keyboard-macros.el" "~"))

(setq warning-minimum-level :emergency)
(setq warning-minimum-log-level :emergency)
(setq warning-suppress-log-types '(elpy))
(setq-default flycheck-disabled-checkers '(python-flake8 python-pylint))

;;(require 'cider)

(paredit-mode 1)

(require 'ido)
(ido-mode t)

;; Place all AutoSave files in one directory:
(setq backup-directory-alist
      `(("." . ,(concat user-emacs-directory "backups"))))

;; use CTRL-, as CTRL-x (to give some rest to your left hand...):
(define-key function-key-map [?\C-.] [?\C-x])
(define-key function-key-map [?\C-,] [?\C-c])

;; example: (setq location-on-keyboard 'keybard-functionality)
;; Alt as Meta, needed bc I set the Win key to Meta at OS level, see
;; in ~/.Xmodmap file:
(setq x-alt-keysym 'meta)
(setq x-altgr-keysym 'meta)

(setq x-hyper-keysym 'ctrl)
(setq x-super-keysym 'ctrl) ;
;(setq x-window-keysym 'ctrl)
					; 

;; these are the WRONG order:
					;(setq x-ctrl-keysym 'hyper) ; 
					;(setq x-ctrl-keysym 'super) ; win on wifi keyboard?
