;; Ez egy emacs init.el fáj, aminek legfrissebb verziója innen letölthető:
;; https://gitlab.com/atmamta2/emacs_init.el git repoval,

;;;;;;;;;;;;;;;; key maps:
;; use CTRL-, as CTRL-x (to give some rest to your left hand...):
(define-key function-key-map [?\C-.] [?\C-x])
(define-key function-key-map [?\C-,] [?\C-c])

;; example: (setq location-on-keyboard 'keybard-functionality)
;; Alt as Meta, needed bc I set the Win key to Meta at OS level, see
;; in ~/.Xmodmap file:
(setq x-alt-keysym 'meta)
(setq x-altgr-keysym 'meta)

(setq x-hyper-keysym 'ctrl)
(setq x-super-keysym 'ctrl);
;(setq x-window-keysym 'ctrl)
					; 

;; these are the WRONG order:
					;(setq x-ctrl-keysym 'hyper) ; 
					;(setq x-ctrl-keysym 'super) ; win on wifi keyboard?
;;;;;;;;;;;;;;;;

(with-eval-after-load 'package
  (add-to-list 'package-archives
               '("melpa" . "https://melpa.org/packages/")
	       '("gnu" . "https://elpa.gnu.org/packages/")
	       ))

(with-eval-after-load 'package
  (add-to-list 'package-archives
	       '("nongnu" . "https://elpa.nongnu.org/nongnu/")))

;(require 'package)
;(package-initialize)

;;(eval-when-compile
;;  ;; Following line is not needed if use-package.el is in ~/.emacs.d
;;  (add-to-list 'load-path "/home/attila/.emacs.d/elpa/")
;;  (require 'use-package))

(eval-when-compile
  (require 'use-package))
; (require 'diminish)                ;; if you use :diminish
(require 'bind-key)                  ;; if you use any :bind variant

(use-package auto-package-update
  :ensure t
  :config
  (setq auto-package-update-interval 14
	auto-package-update-delete-old-versions t
	auto-package-update-hide-results t
	auto-package-update-prompt-before-update t)
  (auto-package-update-maybe))

(add-hook 'auto-package-update-before-hook
          (lambda () (message "I will update packages now")))
;;;;; MANUAL package update:
;(package-refresh-contents)
;(auto-package-update-now-async)

(defvar my-packages '(;use-package
		      org
		      elpy     ;; advanced python mode
		      lsp-mode
		      which-key
		      dap-mode
		      ;; dap-python
		      ;; python-mode
                      ;; ;;el-get   ;; emacs elisp external package manager
                      ;; better-defaults
                      ;; ;;projectile  ;; project management
                      ;; ;; clojure-mode
		      smartparens
                      flycheck ;; syntax check on the fly
                      ;; ;; pyflakes
                      ;; py-autopep8 ;; python buffer beautifier using autopep
                      ;; ;; egg      ;; emacs got git - git emacs integration
                      anaconda-mode ;; show doc, def, etc at point
                      rainbow-delimiters  ;; highlights the other side of brackets
                      company
                      pyvenv   ;; !!needs pyton3-venv??
                      find-file-in-project
                      s
                      importmagic
                      ))

(dolist (p my-packages)
  (unless (package-installed-p p)
    (package-install p)))

;;_________________clojure_____________________
;; (add-hook 'clojure-mode-hook
;;           '(lambda ()
;;              (paredit-mode 1)
;;              ;; Actual keyboard bindings follow:
;;              ;;Start Cider REPL with control-o-j:
;;              (define-key clojure-mode-map (kbd "C-o j") 'cider-jack-in)
;;              ;;Restart Cider REPL with control-o-J (in case of dirty JVM, etc.):
;;              (define-key clojure-mode-map (kbd "C-o J") 'cider-restart)
;;              (define-key clojure-mode-map (kbd "s-i") 'cider-eval-last-sexp)
;;              (define-key clojure-mode-map (kbd "C-o y") 'cider-eval-last-sexp-and-append)))

;; ;; Append result of evaluating previous expression (Clojure):
;; (defun cider-eval-last-sexp-and-append ()
;;   "Evaluate the expression preceding point and append result."
;;   (interactive)
;;   (with-current-buffer (current-buffer)
;;       (insert ";;=>")
;;       (eval-print-last-sexp)
;;       )
;;     )



;;_________________python elpy_____________________
; note: documentation of elpy mode is here:
; http://elpy.readthedocs.io/en/latest/ide.html
;; (setq path-to-python-executable "/usr/bin/python")
;; !!Needs:
;; 1. pip install setuptools
;; 2. apt install python3-venv
;; !!


;; !!!Edit the computer names and the paths to the python executable on these systems in the following!!!:

;; ;; ;; python 3 env:
;; (setq anaconda-root-dir (expand-file-name "anaconda3" "~")) ;;AAA
;; (setq anaconda-bin-dir (expand-file-name "bin" anaconda-root-dir)) ;; Linux
;; (setq python-executable-str1 (expand-file-name "python" anaconda-bin-dir))
;; (setq python-executable-str2  (expand-file-name "ipython" anaconda-bin-dir)) 
;; (setq local-bin-dir (expand-file-name "usr/bin/" "~"))  ;;AAA

;; (setq pydpl-project-root (expand-file-name "work/ml/neural_network_dir/pydpl" "~")) ;;AAA
;; (setq audio-project-root (expand-file-name "work/ml/neural_network_dir/pydpl/audio" "~")) ;;AAA
;; (setq vision-project-root (expand-file-name "work/ml/neural_network_dir/pydpl/vision" "~")) ;;AAA

;(add-to-list 'exec-path anaconda-bin-dir)  ;;AAA
;(add-to-list 'exec-path vision-project-root) ;;AAA
;(add-to-list 'exec-path audio-project-root) ;;AAA
;(add-to-list 'exec-path local-bin-dir)  ;;AAA

;; (defun eval-after-restart-python-console ()
;;      "Restart python console before evaluate buffer or region to avoid various uncanny conflicts, like not reloding modules even when they are changed"
;;      (interactive)
;;      (if (get-buffer "*Python*")
;; 	 (let ((kill-buffer-query-functions nil)) (kill-buffer "*Python*"))
;;        (split-window-horizontally (floor (* 0.75 (window-width)))))
;;      (elpy-shell-send-region-or-buffer)
;;      (enlarge-window-horizontally 20))

;(global-set-key (kbd "C-c C-x C-c") 'eval-after-restart-python-console)

(use-package smartparens
  :ensure t
  :hook ((text-mode prog-mode python-mode) . smartparens-mode))

(use-package company
  :ensure t
  :hook
  ((text-mode prog-mode python-mode elpy-mode) . company-mode)
  :config
  (setq company-idle-delay 0.1
	company-minimum-prefix-length 1))

(defun eval-after-restart-python-console ()
  "Restart python console before evaluate buffer or region to avoid various uncanny conflicts, like not reloding modules even when they are changed"
  (interactive)
  (if (get-buffer "*Python*")
      (let ((kill-buffer-query-functions nil)) (kill-buffer "*Python*"))
    (split-window-horizontally (floor (* 0.75 (window-width)))))
  (elpy-shell-send-region-or-buffer)
  (enlarge-window-horizontally 20))

(use-package python-mode
  :ensure t
  :defer t
  :mode ("\\.py\\'" . python-mode)
  :hook ((python-mode . elpy-mode)
	 (python-mode . anaconda-mode)
	 (python-mode . smartparens-mode)
	 (python-mode . flycheck-mode)
	 )
  :init
  (setq anaconda-root-dir (expand-file-name "anaconda3" "~") ;;AAA
	anaconda-bin-dir (expand-file-name "bin" anaconda-root-dir) ;; Linux
	python-executable-str1 (expand-file-name "python" anaconda-bin-dir)
	path-to-python-executable anaconda-bin-dir
	python-shell-interpreter python-executable-str1
	local-bin-dir (expand-file-name "usr/bin/" "~") ;;AAA
	elpy-rpc-python-command python-executable-str1

	pydpl-project-root (expand-file-name "work/ml/neural_network_dir/pydpl" "~") ;;AAA
	audio-project-root (expand-file-name "work/ml/neural_network_dir/pydpl/audio" "~") ;;AAA
	vision-project-root (expand-file-name "work/ml/neural_network_dir/pydpl/vision" "~") ;;AAA
	elpy-project-root pydpl-project-root
	)
  (add-to-list 'exec-path anaconda-bin-dir)  ;;AAA
  (add-to-list 'exec-path vision-project-root) ;;AAA
  (add-to-list 'exec-path audio-project-root) ;;AAA
  (add-to-list 'exec-path local-bin-dir)  ;;AAA
	
  (advice-add 'python-mode :before 'elpy-enable)
  :config
  ;(setq python-indent-offset 4)
  (setq flymake-gui-warnings-enabled nil
	flycheck-disabled-checkers '(python-flake8 python-pylint flymake))
  ;(flymake-mode)
  :bind ("C-c C-x C-c" . eval-after-restart-python-console)
  )

(use-package elpy
  :ensure t
  :defer t
  :after python-mode
  ;:mode (("\\.py\\'" . elpy-mode)
  ;;:hook (elpy-mode . ...)
  :init ; before package is loaded:
  (setq ;; elpy-modules '(elpy-module-company elpy-module-eldoc elpy-module-pyvenv
	;; 				   elpy-module-highlight-indentation
	;; 				   elpy-module-yasnippet ;elpy-module-django
	;; 				   elpy-module-sane-defaults)
        ;; elpy-project-root pydpl-project-root
	;; elpy-rpc-python-command python-executable-str1
	;; add-to-list 'exec-path anaconda-bin-dir  ;;AAA
	;; add-to-list 'exec-path local-bin-dir;;AAA
	)
  ;; (when (require 'flycheck nil t)
  ;;   (setq elpy-modules (delq 'elpy-module-flymake elpy-modules)))
  ;; (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (elpy-enable)
  :config ; after package is loaded:
  ;(flymake-mode)
  )



(setq ansi-color-for-comint-mode t)

;; (setq path-to-python-executable anaconda-bin-dir
;;       python-shell-interpreter  python-executable-str1 
;;       ;;python-shell-interpreter-args "-i"
;;       )

;; ;; (cond ((string= system-name "alfa")
;; ;;        (setq path-to-python-executable python-executable-str2)
;; ;;        ;(pyvenv-activate "/home/attila/usr/local/lib/anaconda3/bin/")
;; ;;        )
;; ;;       ((string= system-name "hulk4")
;; ;;        (setq path-to-python-executable python-executable-str2)
;; ;;        ;(pyvenv-activate "/home/attila/usr/local/lib/anaconda3/bin/")
;; ;;        ))

;; (add-hook 'python-mode-hook 'elpy-mode)
;; (add-hook 'python-mode-hook 'anaconda-mode)
;; (add-hook 'python-mode-hook 'anaconda-eldoc-mode)
;; ;; (add-hook 'python-mode-hook 'paredit-mode)
;; (add-hook 'python-mode-hook 'flycheck-mode)




;; (add-hook 'elpy-mode-hook
;;           '(lambda ()
;;              (anaconda-mode 1)
;; ;;              (paredit-mode 1)
;;              (flycheck-mode 1)
;;              (flymake-mode 0)
;;              ;;(elpy-use-ipython python-executable-str2)
;;              ;;(elpy-use-cpython python-executable-str1)
;; 	     ))





;; ;;  ;; use flycheck not flymake with elpy
;; (when (require 'flycheck nil t)
;;   (setq elpy-modules (delq 'elpy-module-flymake elpy-modules)))

;; ;; enable autopep8 formatting on save
;; (require 'py-autopep8)
;; (add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)

(defun company-yasnippet-or-completion ()
  "Solve company yasnippet conflicts."
  (interactive)
  (let ((yas-fallback-behavior
         (apply 'company-complete-common nil)))
    (yas-expand)))

(add-hook 'company-mode-hook
          (lambda ()
            (substitute-key-definition
             'company-complete-common
             'company-yasnippet-or-completion
             company-active-map)))




(setq tramp-default-method "ssh")

(defun toggle-fullscreen ()
  (interactive)
  (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
                 '(2 "_NET_WM_STATE_MAXIMIZED_VERT" 0))
  (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
                 '(2 "_NET_WM_STATE_MAXIMIZED_HORZ" 0))
  )
(toggle-fullscreen)


(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:stipple nil :background "black" :foreground "white" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 136 :foundry "PfEd" :family "DejaVu Sans Mono" :width normal))))
 '(font-lock-builtin-face ((((class color) (background dark)) (:foreground "Turquoise"))))
 '(font-lock-comment-face ((t (:foreground "purple"))))
 '(font-lock-constant-face ((((class color) (background dark)) (:bold t :foreground "DarkOrchid"))))
 '(font-lock-doc-string-face ((t (:foreground "green2"))))
 '(font-lock-function-name-face ((t (:foreground "yellow"))))
 '(font-lock-keyword-face ((t (:bold t :foreground "orange"))))
 '(font-lock-preprocessor-face ((t (:italic nil :foreground "CornFlowerBlue"))))
 '(font-lock-reference-face ((t (:foreground "DodgerBlue"))))
 '(font-lock-string-face ((t (:foreground "orange"))))
 '(font-lock-type-face ((t (:foreground "#9290ff"))))
 '(font-lock-variable-name-face ((t (:foreground "PaleGreen"))))
 '(font-lock-warning-face ((((class color) (background dark)) (:foreground "yellow" :background "red"))))
 '(highlight ((t (:background "gray5"))))
 '(list-mode-item-selected ((t (:background "gold"))))
 '(makefile-space-face ((t (:background "wheat"))) t)
 '(mode-line ((t (:background "Navy"))))
 '(paren-match ((t (:background "darkseagreen4"))))
 '(region ((t (:background "DarkSlateBlue"))))
 '(show-paren-match ((t (:foreground "red" :background "black"))))
 '(show-paren-mismatch ((((class color)) (:foreground "white" :background "red"))))
 '(speedbar-button-face ((((class color) (background dark)) (:foreground "green4"))))
 '(speedbar-directory-face ((((class color) (background dark)) (:foreground "khaki"))))
 '(speedbar-file-face ((((class color) (background dark)) (:foreground "cyan"))))
 '(speedbar-tag-face ((((class color) (background dark)) (:foreground "Springgreen"))))
 '(vhdl-speedbar-architecture-selected-face ((((class color) (background dark)) (:underline t :foreground "Blue"))))
 '(vhdl-speedbar-entity-face ((((class color) (background dark)) (:foreground "darkGreen"))))
 '(vhdl-speedbar-entity-selected-face ((((class color) (background dark)) (:underline t :foreground "darkGreen"))))
 '(vhdl-speedbar-package-face ((((class color) (background dark)) (:foreground "black"))))
 '(vhdl-speedbar-package-selected-face ((((class color) (background dark)) (:underline t :foreground "black")))))

(setq my-unignored-buffers '("*scratch*" "*Python*" "*Messages*"))

(defun my-ido-ignore-func (name)
  "Ignore all non-user (a.k.a. *starred*) buffers except those listed in `my-unignored-buffers'."
  (and (string-match "^\*" name)
       (not (member name my-unignored-buffers))))

;(setq ido-ignore-buffers '("\\` " my-ido-ignore-func))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(elpy-modules
   '(elpy-module-company elpy-module-eldoc elpy-module-folding elpy-module-pyvenv elpy-module-highlight-indentation elpy-module-yasnippet elpy-module-django elpy-module-sane-defaults))
 '(package-selected-packages
   '(which-key use-package rainbow-delimiters pyvenv-auto python-mode pkg-info paredit lsp-jedi importmagic gnu-elpa-keyring-update flycheck find-file-in-project elpy dap-mode better-defaults auto-package-update anaconda-mode))
 '(python-indent-offset 4)
 '(show-paren-mode t)
 '(tab-stop-list
   '(4 8 12 16 20 24 28 32 36 40 44 48 52 56 64 72 80 88 96 104 112 120))
 '(tool-bar-mode nil))

(load-file (expand-file-name ".emacs.d/elpa/keyboard-macros.el" "~"))

(setq warning-minimum-level :emergency)
(setq warning-minimum-log-level :emergency)
(setq warning-suppress-log-types '(elpy))
(setq-default flycheck-disabled-checkers '(python-flake8 python-pylint))

;;(require 'cider)

(require 'ido)
(ido-mode t)

;; Place all AutoSave files in one directory:
(setq backup-directory-alist
      `(("." . ,(concat user-emacs-directory "backups"))))

